package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"math"
	"math/rand"
	"net/http"
	"os"
	"time"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func MakeRequest(url string, ch chan<-struct {time.Time; float64}, userId int, client *http.Client) {
	values := map[string]int{"user_id": userId}
	jsonValue, err := json.Marshal(values)
	check(err)

	start := time.Now()
	req, err := http.NewRequest("GET", url,  bytes.NewBuffer(jsonValue))
	check(err)
	req.Header.Set("Content-Type", "application/json")
	resp, err := client.Do(req)
	check(err)

	secs := time.Since(start).Seconds()
	_, err = ioutil.ReadAll(resp.Body)
	check(err)
	ch <- struct {time.Time; float64}{start, secs}
}

func main() {
	start := time.Now()
	ch := make(chan struct {time.Time; float64})
	requestsCount := flag.Int("count", 1, "requests count")
	usersFile := flag.String("users", "users.txt", "users file")
	usersCount := flag.Int("users_count", 0, "users count")
	usersOffset := flag.Int("users_offset", 0, "users offset")
	url := flag.String("url", "", "users file")
	resultFile := flag.String("result", "output", "times file")
	p := flag.Int("p", 1, "ps")

	flag.Parse()

	_, err := ioutil.ReadFile(*usersFile)
	check(err)

	users := make([]int, *usersCount)
	for i := 0; i < *usersCount; i++ {
		users[i] = i + *usersOffset
	}

	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(users), func(i, j int) {
		users[i], users[j] = users[j], users[i]
	})

	f, err := os.Create(*resultFile)
	check(err)

	defer f.Close()

	var min, max, sum float64 = math.Inf(1), 0, 0

	client := &http.Client{}

	starts := make([]time.Time, *requestsCount * *p)
	durations := make([]float64, *requestsCount * *p)
	for i := 0; i < *requestsCount; i++ {
		for j := 0; j < *p; j++ {
			userId := users[((i**p)+j)%len(users)]
			go MakeRequest(*url, ch, userId, client)
		}
		for j := 0; j < *p; j++ {
			pair := <-ch
			star := pair.Time
			cur := pair.float64
			sum += cur
			if min > cur {
				min = cur
			}
			if max < cur {
				max = cur
			}
			starts[i**p+j] = star
			durations[i**p+j] = cur
			_, err = f.WriteString(fmt.Sprintf("%s %f\n", star.Format("2006-01-02 15:04:05"), cur))
			check(err)
		}
	}

	fmt.Printf("Latency: %f\n", sum / float64(*requestsCount * *p))


	fmt.Printf("%.2fs elapsed\n", time.Since(start).Seconds())
}