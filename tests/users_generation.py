import asyncio
import json
from urllib.parse import urljoin
from werkzeug.security import generate_password_hash
import os
import argparse
import random
import typing as tp
import aiohttp
import pathlib
import ujson


Links = tp.List[str]
DEBUG = False


class User:
    def __init__(self, user_id: int, followers: tp.List[int], posts: Links):
        self.user_id: int = user_id
        self.global_id: int = user_id + 25
        self.followers: tp.List[int] = followers
        self.posts: Links = posts
        self.name: str = 'test_' + str(user_id)
        self.password: str = 'pwd{}'.format(self.name)
        self.email: str = '{}@test.test'.format(user_id)

    def json(self) -> tp.Dict[str, str]:
        return {
            'email': self.email,
            'name': self.name,
            'password': generate_password_hash(self.password, method='sha256')
        }

    def __str__(self) -> str:
        return "User\n" \
               "    user_id: {}\n" \
               "    global_id: {}\n" \
               "    posts: {}\n" \
               "    followers: {}\n" \
               "    name: {}\n" \
               ")".format(self.user_id, self.global_id, self.posts, self.followers, self.name)


def read_links(filepath: str) -> Links:
    with open(filepath) as f:
        return f.read().splitlines()


def generate_user(user_id: int, posts: int, followers: int, links_list: Links, users_list: tp.List[int]) -> User:
    return User(user_id,
                random.sample(users_list, followers),
                random.sample(links_list, posts))


async def create_user(session: aiohttp.ClientSession, user: User, base_url: str, _: tp.List[User]) -> None:
    async with session.post(urljoin(base_url, 'api/create_user'), json=user.json()) as resp:
        data = await resp.read()
        if not resp.ok:
            print(resp.status)
        user.global_id = json.loads(data)['id']


async def follow_them(session: aiohttp.ClientSession, user: User, base_url: str, users: tp.List[User]) -> None:
    for following in user.followers:
        await session.post(urljoin(base_url, 'api/follow'), json={
            'follower_id': user.global_id,
            'following_id': users[following].global_id
        })


async def post_them(session: aiohttp.ClientSession, user: User, base_url: str, _: tp.List[User]) -> None:
    for poster in user.posts:
        await session.post(urljoin(base_url, 'api/new_post'), json={
            'user_id': user.global_id,
            'urn': poster
        })


async def apply_handler(users: tp.List[User], url: str, handler):
    async with aiohttp.ClientSession(json_serialize=ujson.dumps) as session:
        for i, fut in enumerate(asyncio.as_completed(list(handler(session, user, url, users) for user in users))):
            await fut


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-u', '--users', required=True, type=int,
                        help='number of users')
    parser.add_argument('-f', '--followers', type=int, required=True,
                        help='number of followers')
    parser.add_argument('-p', '--posts', type=int, required=True,
                        help='number of posts')
    parser.add_argument('--url', type=str, required=True)
    parser.add_argument('--user_id_offset', type=int, default=0)
    parser.add_argument('-l', '--links_path', required=True,
                        help='path to the file with links')
    parser.add_argument('-o', '--output', default='output', help='user_ids file')

    args = parser.parse_args()
    if not os.path.exists(os.path.join(os.getcwd(), args.links_path)) and \
            not os.path.exists(args.dir):
        print('no such directory: ', args.dir)
        exit(2)

    links = read_links(args.links_path)
    print("Read links")

    users = [generate_user(i, args.posts, args.followers, links,
                           list(range(args.users))) for i in
             range(args.user_id_offset, args.user_id_offset + args.users)]
    print("Generated users")

    if DEBUG:
        print(*users, sep='\n')

    loop = asyncio.get_event_loop()
#    loop.run_until_complete(apply_handler(users, args.url, create_user))
    print("Created users")
    loop.run_until_complete(apply_handler(users, args.url, follow_them))
    print("Followed users")
    loop.run_until_complete(apply_handler(users, args.url, post_them))
    print("Posted posts")

    with open(args.output, 'w') as f:
        for user in users:
            print(user.global_id, file=f)
    print("Finished")
