import os
import argparse
import typing as tp
import numpy as np
import time
import matplotlib.pyplot as plt

DEBUG = False


def read_lines(filepath: str) -> tp.List[str]:
    with open(filepath) as f:
        return f.read().splitlines()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--times_path', required=True,
                        help='path to the file with times')

    args = parser.parse_args()
    if not os.path.exists(os.path.join(os.getcwd(), args.times_path)):
        print('no such file:')
        exit(2)

    starts, durations = [], []
    for line in read_lines(args.times_path):
        elem = line.split(' ')
        durations.append(float(elem[-1]))
        starts.append(time.strptime(elem[0] + ' ' + elem[1], "%Y-%m-%d %H:%M:%S"))

    durs = dict()
    counts = dict()
    for i in range(len(starts)):
        start = starts[i]
        if start in durs:
            durs[start] += durations[i]
            counts[start] += 1
        else:
            durs[start] = durations[i]
            counts[start] = 1

    means = dict()
    for key in durs.keys():
        means[key] = durs[key] / counts[key]
    print(*means.values())

    plt.plot([str(i) for i in durs.keys()], counts.values())
    #    plt.plot([str(i) for i in durs.keys()], means.values())
    print(max(means.values()))
    plt.show()

    print(counts.values())