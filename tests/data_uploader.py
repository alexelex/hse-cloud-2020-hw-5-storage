from collections import namedtuple
import os
import boto3
from botocore import config
import argparse
import pathlib


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--dir', required=True)
    parser.add_argument('-b', '--bucket', required=True)
    parser.add_argument('-awi', '--aws_access_key_id', required=True)
    parser.add_argument('-awk', '--aws_secret_access_key', required=True)
    parser.add_argument('-l', '--links', help='get list of links',
                        action='store_true')
    args = parser.parse_args()
    if not os.path.exists(args.dir) or not os.path.isdir(args.dir):
        print('no such directory: ', args.dir)
        exit(2)

    Config = namedtuple('Config', ['name', 'URL', 'field'])
    S3Config = Config('s3', 'https://storage.yandexcloud.net', args.bucket)
    S3RegionConfig = config.Config(region_name='ru-central1')

    session = boto3.session.Session()
    s3 = session.client(
        service_name=S3Config.name,
        endpoint_url=S3Config.URL,
        aws_access_key_id=args.aws_access_key_id,
        aws_secret_access_key=args.aws_secret_access_key,
        config=S3RegionConfig
    )

    links = []

    file_id = 0
    for path, _, files in os.walk(args.dir):
        for filename in files:
            file_id += 1
            filepath = os.path.join(path, filename)
            key = "{:08d}".format(file_id) + pathlib.Path(filename).suffix
            s3.upload_file(filepath, S3Config.field, key)
            links.append(os.path.join(S3Config.URL, S3Config.field, key))

    if args.links:
        print(*links, sep='\n')
