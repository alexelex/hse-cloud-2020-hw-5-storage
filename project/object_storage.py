from collections import namedtuple
from typing import Optional
from os import getenv, path
import boto3
from botocore import config
from .logger import get_module_logger

logger = get_module_logger(__name__)
Config = namedtuple('Config', ['name', 'URL', 'field'])
S3Config = Config('s3', 'https://storage.yandexcloud.net', 'alexelex-insta')
S3RegionConfig = config.Config(region_name='ru-central1')

session = boto3.session.Session()
s3 = session.client(
    service_name=S3Config.name,
    endpoint_url=S3Config.URL,
    aws_access_key_id=getenv('AWS_ACCESS_KEY_ID'),
    aws_secret_access_key=getenv('AWS_SECRET_ACCESS_KEY'),
    config=S3RegionConfig
)


def upload_to_storage(stream, filename: str) -> Optional[str]:
    logger.info('uploading file "{}" to object storage '.format(filename))
    try:
        s3.upload_fileobj(stream, S3Config.field, filename)
        return filename
    except Exception as e:
        logger.error(e)


def make_obj_link(key: str) -> str:
    link = path.join(S3Config.URL, S3Config.field, key)
    logger.info(link)
    return link
